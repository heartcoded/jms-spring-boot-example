#JMS
= Java API orientat pe mesaje pentru trimiterea mesajelor intre 2:N clienti (aps sau componente software)

#JmsTemplate
= handles the creation and releasing of resources when sending
or synchronously receiving messages.

#ConnectionFactory (Connection management)
= in order to connect and be able to send/receive messages we need 
to configure a ConnectionFactory

#Destination Management


#Message Conversion
= the default conversion strategy used by JmsTemplate for both ConvertAndSend()
and ReceiveAndConvert() operations is the SimpleMessageConverter class
*The SimpleMessageConverter is able to handle TextMessages, BytesMessages,
MapMessages, and ObjectMessages. This class implements the MessageConverter interface.
Apart from SimpleMessageConverter, Spring JMS provides some other MessageConverter 
classes out of the box like MappingJackson2MessageConverter, 
MarshallingMessageConverter, MessagingMessageConverter.



#Spring JMS Options
1. JmsTemplate - either to send and receive messages inline
    a) Use send()/convertAndSend() methods to send messages
    b) Use receive()/receiveAndConvert() methods to receive
    messages. BEWARE: these are blocking methods! If there is no
    message on the Destination, it will wait until a message is
    received or times out
2. MessageListenerContainer - Async JMS messages receipt by polling
JMS Destinations and directing messages to service methods or MDBs.
It is time to graduate Spring JmsTemplate and play with the big kids. 
We can easily do this with a Spring Integration flow.
    Components of Spring Integration:
    a)Create a Gateway interface - an interface defining methods that accept
    the type of data you wish to send and any optional header values
    b)Define the Channel - the pipe connecting our endpoints
    c)Define an Outbound JMS Adapter - sends the messages to your JMS provider
    (ActiveMQ,RabbitMQ etc);
    
