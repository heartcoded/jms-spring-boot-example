package com.dianabb.learning.jmsspringbootexample.jms;


import com.dianabb.learning.jmsspringbootexample.model.OrderTransaction;
import org.springframework.jms.annotation.JmsListener;
import org.springframework.stereotype.Component;

@Component
public class OrderTransactionReceiver {

    private int index = 1;

    @JmsListener(destination = "DianaQueue",containerFactory = "dianaFactory")
    public void receiveMessage(OrderTransaction transactionReceived){
        
        System.out.println("("+index+") Received: " +transactionReceived);
        index ++;
    }
}
