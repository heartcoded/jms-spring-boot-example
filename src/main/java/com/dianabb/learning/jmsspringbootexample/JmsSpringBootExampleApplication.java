package com.dianabb.learning.jmsspringbootexample;

import com.dianabb.learning.jmsspringbootexample.model.OrderTransaction;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jms.DefaultJmsListenerContainerFactoryConfigurer;
import org.springframework.context.annotation.Bean;
import org.springframework.jms.annotation.EnableJms;
import org.springframework.jms.config.DefaultJmsListenerContainerFactory;
import org.springframework.jms.config.JmsListenerContainerFactory;
import org.springframework.jms.support.converter.MappingJackson2MessageConverter;
import org.springframework.jms.support.converter.MessageConverter;
import org.springframework.jms.support.converter.MessageType;
import org.springframework.util.ErrorHandler;

import javax.jms.ConnectionFactory;

@EnableJms
@SpringBootApplication
public class JmsSpringBootExampleApplication {

	public static void main(String[] args) {
		SpringApplication.run(JmsSpringBootExampleApplication.class, args);
	}

	//required due to defining custom 'dianaFactory' in the receiver
	@Bean
	public JmsListenerContainerFactory<?> dianaFactory(ConnectionFactory connectionFactory, DefaultJmsListenerContainerFactoryConfigurer configurer) {
		DefaultJmsListenerContainerFactory factory = new DefaultJmsListenerContainerFactory();

		//using lambda expressions instead of using anonymous class
		factory.setErrorHandler(t->System.err.println("Ouf...an error has occurred during the transaction :(."));
		configurer.configure(factory,connectionFactory);
		return factory;
	}

	/* serialize message content to json using TextMessage
    default implementation can only convert basic types which the OrderTransaction object is not
	This implementation uses JSON to pass the messages to and from the queue.
	Spring Boot is smart enough to detect the MessageConverter and make use of it in the JmsTemplate and JmsListenerContainerFactory
	*/
	@Bean
	public MessageConverter jacksonJmsMessageConverter() {
		MappingJackson2MessageConverter converter = new MappingJackson2MessageConverter();
		converter.setTargetType(MessageType.TEXT);
		converter.setTypeIdPropertyName("_type");
		return converter;
	}
}
