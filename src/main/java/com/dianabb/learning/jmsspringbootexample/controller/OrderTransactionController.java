package com.dianabb.learning.jmsspringbootexample.controller;


import com.dianabb.learning.jmsspringbootexample.model.OrderTransaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jms.core.JmsTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class OrderTransactionController {

    @Autowired
    private JmsTemplate jmsTemplate;

    @PostMapping("/send-money")
    public void send(@RequestBody OrderTransaction transaction) {
        System.out.println("Sending new transaction...");
        jmsTemplate.convertAndSend("DianaQueue",transaction);
        System.out.println("Message sent.");
    }
}
