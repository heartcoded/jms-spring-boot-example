package com.dianabb.learning.jmsspringbootexample.model;


import lombok.*;
import org.springframework.core.annotation.Order;

import java.math.BigDecimal;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class OrderTransaction {

    private Long id;
    private String from;
    private String to;
    private BigDecimal amount;

}
